import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View ,SafeAreaView} from 'react-native';
import Weather from './Weather';

export default function App() {
  return (
    
      <View style={styles.container}>
       
        <Weather  />
        <StatusBar style="auto" />
      </View>
   
  );
}

const styles = StyleSheet.create({
 
  container: {
    flex: 1,
    backgroundColor: '#E3F1ED',
    alignItems: 'center',
    justifyContent: 'center',
  },
 
});

