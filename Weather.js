import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, TextInput, Button, Image } from 'react-native';


const Weather = () => {
    const [city, setCity] = useState('');
    const [weatherData, setWeatherData] =  useState(null); // Initialize as null
  
    const apiKey = '1ae8b15feafb0fca5d85221557647fdd';
    const apiUrl = 'https://api.openweathermap.org/data/2.5/weather?units=metric&q=';
  
    const weatherIcons = {
      Clouds: require('./assets/images/clouds.png'),
      Clear: require('./assets/images/clear.png'),
      Rain: require('./assets/images/rain.png'),
      Drizzle: require('./assets/images/drizzle.png'),
      Mist: require('./assets/images/mist.png'),
    };
  
    const fetchWeatherData = async () => {
      try {
        const response = await fetch(`${apiUrl}${city}&appid=${apiKey}`);
        if (response.status === 404) {
          setWeatherData(null);
        } else {
          const data = await response.json();
          setWeatherData(data);
        }
      } catch (error) {
        console.error('Error fetching weather data:', error);
      }
    };
  
    return (
      <View style={styles.container}>
        <Text style={styles.heading}> Weather   </Text>
        <View style={styles.search}>
          <TextInput
            style={styles.input}
            placeholder="Enter Your City Name"
            onChangeText={(text) => setCity(text)}
          />
          <Button title="Search" onPress={fetchWeatherData} color={'black'} />
        </View>
        {weatherData && ( // Display only if weatherData is truthy
          <View style={styles.weather}>
            <Image
              source={weatherData.weather && weatherIcons[weatherData.weather[0].main]}
              style={styles.weatherIcon}
            />
            <Text style={styles.temp}>
              {weatherData.main && weatherData.main.temp && Math.round(weatherData.main.temp) + '°C'}
            </Text>
            <Text style={styles.cityName}>{weatherData.name || ''}</Text>
            <View style={styles.details}>  
            <View style={styles.detailsContainer}>
  <View style={styles.col}>
    <Image source={require('./assets/images/humidity.png')} />
    <View>
      <Text style={styles.humidity}>
        {weatherData.main && weatherData.main.humidity && weatherData.main.humidity + '%'}
      </Text>
      <Text>Humidity</Text>
    </View>
  </View>
  <View style={styles.col}>
    <Image source={require('./assets/images/wind.png')} />
    <View>
      <Text style={styles.wind}>
        {weatherData.wind && weatherData.wind.speed && weatherData.wind.speed + ' km/h'} </Text> 
      <Text>Wind Speed</Text>
    </View>
  </View>
</View>

            </View>
          </View>
        )}
        {!weatherData && city && ( // Display error only if city is entered but no data
          <View style={styles.error}>
            <Text>Invalid city name</Text>
          </View>
        )}
      </View>
    );
  };
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },

  heading:{
    fontWeight:'bold',
    fontSize:25,
    marginBottom:50,

  },
  search: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  input: {
    flex: 1,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginRight: 10,
    paddingHorizontal: 10,
  },
  weather: {
    alignItems: 'center',
  },
  weatherIcon: {
    width: 100,
    height: 100,
  },
  temp: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  cityName: {
    fontSize: 18,
    marginBottom: 20,
  },
//   details: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     //width: '90%',
//   },
  col: {
    alignItems: 'center',
    justifyContent: 'space-between', 
    marginBottom: 10, 
    
  },
  humidity: {
    fontSize: 16,
    marginRight:20,
  },
  wind: {
    fontSize: 16,
    marginLeft:20,
  },
  detailsContainer: {
   // flex: 1, 
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 50, 
    alignItems:'center',
    gap:50,
   
  },
  error: {
    marginBottom: 20,
  },
});

export default Weather;